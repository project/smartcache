SmartCache v0.1


What is SmartCache?
-------------------
SmartCache is a js and css gzip & cache script.
It mantains an updated cache of every js and css file on your site and serves it to browsers
supporting http compression.
SmartCache handles Expire Headers too!

SmartCache is a stripped down and polished revision of JSmart, from Ali Farhadi (see
http://farhadi.ir/). Kudos to him for developing this script at first.


Why use SmartCache versus mod_gzip?
-----------------------------------
First and foremost: not every server supports mod_gzip.
If that is the case, SmartCache is for you.
Even if you use have access to mod_gzip, if you cannot configure his caching funcionality or
think they are too limited, you could find SmartCache a usefull little tool.
Or at least, I hope so.


Requirements
------------
1 - Your server must support mod_rewrite to succesfully forward css and js requests to
    smartcache script.
2 - You must have access to .htaccess file.
3 - Your php has to support gzencode function (PHP 4+).
That's it.


How to install - detailed instructions
--------------------------------------
1 - Configure SmartCache settings: open load.php and review the configuration options in the
    head of the file. $offset define the Expires Header timestamp, $baseDir has to point to
    THE ROOT OF YOUR SITE.

2 - Upload the load.php file in your Drupal site. (in /modules/smartcache/).

3 - Give SmartCache read/write permissions on the cache directory (e.g. 775).

4 - Optional: test the script is working. Insert in the address bar of your browser of choice
    http://yoursite/modules/smartcache/load.php?file=misc/drupal.js
    This should give you a copy of drupal.js.
    If that is the case, go on with the next step.
    Else check "About the $baseDir parameter" and "Troubleshooting" sections below.

5 - Redirect js and css requests to SmartCache.
    To do so, you need to edit your site's .htaccess file with the provided rule:
    
    RewriteRule ^(.*\.((js)|(css)))$ /modules/smartcache/load.php?file=$1
    
    It's important to SET THIS RULE AT SITE'S LEVEL. As always, modify the path if needed.
    This will redirect EACH and EVERY js and css request to SmartCache.

6 - Empty your browser cache and open your site: if all is ok, you shouldn't
    notice nothing unusual, except css and js are now 1/4th of their original size!


About the $baseDir parameter
----------------------------
Depending on your Apache's configuration, the $baseDir parameter could be either an absolute or relative path.
My personal host is set to redirect the absolute path "/" to my site's root. So, on my personal site I set:

$baseDir = "/";

If this is not your case, you should probably use a relative path. Keep in mind that:
1 - the path is relative to smartcache dir (... /modules/smartcache/).
2 - the css and js requests sent by client's browser are always sent as absolute paths from your site root.
3 - usually, $baseDir has to point to the same sites root.
4 - .htaccess RewriteBase directive could change the string passed to load.php.
    You should take account of this and change accordingly $baseDir.


Example 1 - Drupal root is site's root:
on client-side page:
<link type="text/css" rel="stylesheet" media="all" href="/sites/default/files/css/9dff4132dc1784483c1e3369dcfd3dff.css" />

The link is absolute, from the root of my site. To point to the root, $baseDir should be (relative to /modules/smartcache/):
$baseDir = "../../";


Example 2 - Drupal on /drupal/ directory:
on client-side page:
<link type="text/css" rel="stylesheet" media="all" href="/drupal/sites/default/files/css/259aac9d40d56b900b7e633911a28485.css" />

load.php now receives a request for "drupal/sites/default/files/css/...".
You have to provide an additional "../":
$baseDir = "../../../";



Example 3 - use of RewriteBase:
on client-side page:
<link type="text/css" rel="stylesheet" media="all" href="/drupal/sites/default/files/css/259aac9d40d56b900b7e633911a28485.css" />

on .htaccess:
RewriteBase /drupal

The RewriteBase directive transforms the link passed to load.php in:
"sites/default/files/css/259aac9d40d56b900b7e633911a28485.css"

So, you have to point to Drupal root:
$baseDir = "../../";

If you experience problems, enable debug (see Troubleshooting).


Troubleshooting
---------------
You can enable "debug" mode setting $debug option on load.php.
Try to open directly a js or css file (say, http://yoursite.name/misc/drupal.js).
In case of errors, it will give you some descriptive message on the problem encountered.

If you still have problems, open an issue.
Any feedback (good or bad) is ALWAYS appreciated.


To do
-----
This is the first release of SmartCache, so some large-scale testing is still needed.
As always, any feedback (either feature request, bug report, etc.) is greatly appreciated.

1 - Add an .info file to make Drupal aware of this script.
2 - I should probably add an automatic minification of css/js files. But, ATM, the best solution in terms of efficiency is to use YUI Compressor by Julien Lecomte and gzip the resultant files. So, this is not a priority.
3 - I should probably make a drupal configuration interface, but hardcoded values are much more efficient and much more KISS.

// 2008-05-15 - Edited 2008-05-23